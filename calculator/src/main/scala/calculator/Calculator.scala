package calculator

sealed abstract class Expr
final case class Literal(v: Double) extends Expr
final case class Ref(name: String) extends Expr
final case class Plus(a: Expr, b: Expr) extends Expr
final case class Minus(a: Expr, b: Expr) extends Expr
final case class Times(a: Expr, b: Expr) extends Expr
final case class Divide(a: Expr, b: Expr) extends Expr

object Calculator {
  def computeValues(namedExpressions: Map[String, Signal[Expr]]): Map[String, Signal[Double]] =
  {
    namedExpressions.mapValues(v => Signal(eval(v(), namedExpressions)))
  }

  def eval(expr: Expr, references: Map[String, Signal[Expr]]): Double =
  {
    def innerLoop(expr: Expr, variables: Set[String]): Double = {
      expr match {
        case Literal(value) => value
        case Ref(name) =>
          {
            if (variables.contains(name) || !references.contains(name))
              Double.NaN
            else
              innerLoop(references(name)(), variables + name)
          }
        case Plus(a, b) => innerLoop(a, variables) + innerLoop(b, variables)
        case Minus(a, b) => innerLoop(a, variables) - innerLoop(b, variables)
        case Times(a, b) => innerLoop(a, variables) * innerLoop(b, variables)
        case Divide(a, b) => innerLoop(a, variables) / innerLoop(b, variables)
        case _ => Double.NaN
      }
    }

    innerLoop(expr, Set())
  }

  /** Get the Expr for a referenced variables.
   *  If the variable is not known, returns a literal NaN.
   */
  private def getReferenceExpr(name: String,
      references: Map[String, Signal[Expr]]) = {
    references.get(name).fold[Expr] {
      Literal(Double.NaN)
    } { exprSignal =>
      exprSignal()
    }
  }
}
