package reductions

import scala.annotation._
import org.scalameter._
import common._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
    def innerBalance(sentence: Array[Char], numberOfOpens: Int): Boolean =
    {
      if (sentence.isEmpty) numberOfOpens == 0
      else
      {
        val singleChar = sentence.head
        val newCounter =
        {
          if (singleChar == '(') numberOfOpens + 1
          else if (singleChar == ')') numberOfOpens - 1
          else numberOfOpens
        }

        if (newCounter >= 0) innerBalance(sentence.tail, newCounter)
        else false
      }
    }

    innerBalance(chars, 0)
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(idx: Int, until: Int, bal: Int, sign: Int): (Int, Int) = {
      if (idx >= until)
        (bal, sign)
      else
      {
        val (newBal, newSign) = chars(idx) match {
          case '(' => (bal+1, if (sign == 0) +1 else sign)
          case ')' => (bal-1, if (sign == 0) -1 else sign)
          case _ => (bal, sign)
        }
        traverse(idx + 1, until, newBal, newSign)
      }
    }

    def reduce(from: Int, until: Int): (Int, Int) = {
      if (until <= from || until - from <= threshold)
        traverse(from, until, 0, 0)
      else
      {
        val mid = from + (until - from)/2
        val ((balanceLeft, signLeft),(balanceRight, signRight)) = parallel(reduce(from, mid), reduce(mid, until))
        val phraseSign = if (signLeft == 0) signRight else signLeft
        (balanceLeft + balanceRight, phraseSign)
      }
    }

    val (balance, sign) = reduce(0, chars.length)
    balance == 0 && sign >= 0
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
