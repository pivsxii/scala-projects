package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    i <- arbitrary[Int]
    j <- oneOf(const(empty), genHeap)
  } yield insert(i, j)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("findMin") = forAll { a: Int =>
    val newHeap = insert(a, empty)
    findMin(newHeap) == a
  }

  property("deleteMin") = forAll { a: Int =>
    val newHeap = insert(a, empty)
    isEmpty(deleteMin(newHeap))
  }

  property("findMaxAmongTwo") = forAll { (x: Int, y: Int) =>
    val newHeap = insert(x, insert(y, empty))
    findMin(deleteMin(newHeap)) == scala.math.max(x, y)
  }

  property("findMaxAmongThree") = forAll { (x: Int, y: Int, z: Int) =>
    val newHeap = insert(x, insert(y, insert(z, empty)))
    findMin(deleteMin(deleteMin(newHeap))) == scala.math.max(x, scala.math.max(y, z))
  }

  property("meld") = forAll { (x: H, y: H) =>
    val min = findMin(meld(x, y))
    min == scala.math.min(findMin(x), findMin(y))
  }

  property("isSorted") = forAll { elem: H =>
    def isSorted(min: Int, h: H): Boolean =
      if (isEmpty(h))
        true
      else if(min > findMin(h))
        false
      else
        isSorted(findMin(h), deleteMin(h))

    isSorted(findMin(elem), deleteMin(elem))
  }
}
