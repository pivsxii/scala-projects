var originalList = List(('a', 3), ('b', 2))
var other = List(('a', 1), ('b', 1))

type Occurrences = List[(Char, Int)]

def subtract(x: Occurrences, y: Occurrences): Occurrences = {
  val (p1,p2) = x.partition(a => y.exists(b => a._1 == b._1))
  val diffed = for( (a,b) <- p1.zip(y) if a._2 != b._2) yield (a._1,a._2-b._2)
  (p2 ++ diffed).sorted
}

def subtractPaolo(x: Occurrences, y: Occurrences): Occurrences = {
  ( x ++ y ).groupBy( _._1 ).map{
    case (key,tuples) => (key, tuples.map( _._2) sum  )
  }.toList
}

subtract(originalList, other)
subtractPaolo(originalList, other)


val (p1,p2) = originalList.partition(a => other.exists(b => a._1 == b._1))
val diffed = for( (a,b) <- p1.zip(other) if a._2 != b._2) yield (a._1,a._2-b._2)
(p2 ++ diffed).sorted