package recfun

object Main {
  def main(args: Array[String]){
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int =
  {
    if (c == 0 || c >= r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {
    def innerBalance(sentence: List[Char], numberOfOpens: Int): Boolean =
    {
      if (sentence.isEmpty) numberOfOpens == 0
      else
      {
        val singleChar = sentence.head
        val newCounter =
        {
          if (singleChar == '(') numberOfOpens + 1
          else if (singleChar == ')') numberOfOpens - 1
          else numberOfOpens
        }

        if (newCounter >= 0) innerBalance(sentence.tail, newCounter)
        else false
      }
    }

    innerBalance(chars, 0)
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int =
  {
    def countChangeInner(money: Int, coins: List[Int]): Int =
    {
      if (money == 0) 1
      else if (money > 0 && !coins.isEmpty) countChangeInner(money - coins.head, coins) + countChangeInner(money, coins.tail)
      else 0
    }

    countChangeInner(money, coins)
  }
}
